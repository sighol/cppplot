M = 6.3;
B11 = 0.2;
A11 = M;
MA = A11 + M;

A = [0 1 0;
	0 -B11/MA 0;
	-0.1 0 0];

B = [0;
	1/MA
	0];

Q = [250 0 0;
	0 5e7 0;
	0 0 15e-6];

R = 1e7;

K = lqr(A, B, Q, R);


format short eng;

K_n = K'
13e-7