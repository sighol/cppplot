#!/usr/bin/env python

from argparse import ArgumentParser
import os, os.path

class colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''

class Compiler:

	def __init__(self, file_path):
		self.file_path = file_path
		self.header_file_path = file_path.replace(".c", ".h")
		self.has_header = os.path.isfile(self.header_file_path)

		self.pfuns = []
		self.efuns = []
		self.compiled = ""
		self.lines = []

		self.get_lines()
		self.get_funs()
		self.compile()

	def get_lines(self):
		with open(self.file_path, 'r') as file:
			self.lines = file.readlines()

	def get_funs(self):
		for i, line in enumerate(self.lines):
			if line == "///pfun\n":
				self.pfuns.append(self.get_string(i+1, "{"))
			if line == "///efun\n":
				self.efuns.append(self.get_string(i+1, "{"))

	def get_string(self, line_start, end_string):
		text = []
		for i in range(line_start, len(self.lines)):
			line = self.lines[i]
			index = line.find(end_string)
			if index != -1:
				text.append(line[:index])
				break
			else:
				text.append(line)
		return "".join(text)

	def compile(self):
		self.compile_source()
		self.compile_header()

	def compile_source(self):
		pfuns = [x.strip() + ";\n" for x in self.pfuns]
		before_index = self.lines.index("///private\n")
		after_index = self.lines.index("///endprivate\n")
		compiled_array = self.lines[:before_index+1] +pfuns +self.lines[after_index:]
		self.compiled = "".join(compiled_array)

	def compile_header(self):
		lines = []
		efuns = [x.strip() + ";\n" for x in self.efuns]
		if self.has_header:
			with open(self.header_file_path, "r") as file:
				lines = file.readlines()
			before_index = lines.index("///public\n")
			after_index = lines.index("///endpublic\n")
			compiled_array = lines[:before_index+1] + efuns +lines[after_index:]
			self.compiled_header = "".join(compiled_array)



def recurse(dir_path):
	for root, dirs, files in os.walk(dir_path):
		c_files = [x for x in files if x.endswith(".c")]
		for c_file in c_files:
			file_path = os.path.join(root, c_file)
			print("{0}Compilerer fil:{1} {3}{2}{4}".format(
				colors.OKBLUE,
				colors.ENDC,
				c_file,
				colors.OKGREEN,
				colors.ENDC,
			))
			try_compile(file_path)

def try_compile(file_path):
	try:
		comp = Compiler(file_path)
		with open(file_path, 'w') as file:
			file.write(comp.compiled)
		if comp.has_header:
			with open(comp.header_file_path, "w") as file:
				file.write(comp.compiled_header)
	except Exception as e:
		print(e)
		print("{0}NOT OK{1}: {2}".format(
			colors.FAIL,
			colors.ENDC,
			file_path,
		))
		print()




if __name__ == "__main__":
	parser = ArgumentParser()
	parser.add_argument("file_path")
	parser.add_argument("-r", "--recursive", action="store_true", help="Do it recursivly on all .c-files in directory")
	args = parser.parse_args()
	if args.recursive:
		if not os.path.isdir(args.file_path):
			exit(0)
		else:
			recurse(args.file_path)
	else:
		comp = Compiler(args.file_path)
		print(comp.compiled)