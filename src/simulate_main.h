#include "graphics/graphics.hpp"
#include "graphics/view.hpp"
#include "graphics/graph.hpp"
#include "graphics/glututil.hpp"
#include "GL/freeglut.h"


#include <stdbool.h>

class Main{
public:
	Graph velocityGraph;
	Graph powerGraph;
	Graph positionGraph;
	Boat boatPlot;

	int elapsed_time;

	double setpoint;

	double next;
	int width, height;
	View view;

	double power_p, power_i, power_d, power;

public:

	Main();

	double getSetpoint() const;
	void setSetpoint(double value);
};


///public
int main(int argc, char* argv[]);
Main* main_new(void);
void keyboard_callback(unsigned char key, int x, int y);
void reshape_callback(int width, int height);
void idle_callback(void);
void display_callback(void);
void simulate_controller(Main* self);
void simulate_apply_power(Main* self, double* pos, double* vel, double power);
void close_callback(void);
///endpublic

