project(anvendt)
cmake_minimum_required(VERSION 2.8)

add_definitions(-Wall)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c11")
set(CMAKE_C_COMPILER g++)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_SOURCE_DIR}/cmake/Modules)

find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)
find_package(GLEW REQUIRED)
find_package(PHIDGETS REQUIRED)
find_library(M_LIB m)
find_package(Eigen3 REQUIRED)


set(CONTROLLER_SRC
	controller/controller.c
	controller/derivator.c
	controller/motor.c
	controller/sensor.c
)

set(GRAPHICS_SRC
	graphics/boat.cpp
	graphics/buffer.cpp
	graphics/glututil.cpp
	graphics/graph.cpp
	graphics/graphics.cpp
	graphics/history.cpp
	graphics/shaderprogram.cpp
	graphics/text.cpp
	graphics/view.cpp

	graphics/plot.hpp
)

set(UTIL_SRC
	util/util.c
	util/optparse.c
)

list(APPEND LIB_SRC
	# ${CONTROLLER_SRC}
	${GRAPHICS_SRC}
	${UTIL_SRC}
)

include_directories("." ${EIGEN3_INCLUDE_DIR})

add_library(anvendt STATIC ${LIB_SRC})
target_link_libraries(anvendt
	${OPENGL_LIBRARIES}
	${GLUT_LIBRARY}
	${GLEW_LIBRARY}
	${M_LIB})

add_executable(simulation simulate_main.cpp)
target_link_libraries(simulation anvendt)

add_executable(test-option tests/test-option.c)
target_link_libraries(test-option anvendt)