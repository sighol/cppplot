#pragma once

#include <stdio.h>
#include "graphics/graphics.h"
#include "graphics/graph.h"
#include "graphics/view.h"
#include "graphics/glututil.h"
#include "util/optparse.h"
#include "GL/freeglut.h"


typedef struct {
	Graph* graph;

	double last;
	double next;
	int width, height;
	View* view;
} Main;

///public
int main(int argc, char* argv[]);
Main* main_new(void);
void idle_callback(void);
void keyboard_callback(unsigned char key, int x, int y);
void reshape_callback(int width, int height);
void display_callback(void);
///endpublic
