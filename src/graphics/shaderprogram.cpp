#include "shaderprogram.hpp"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>


static char* vert_shaderprogram_pass = "#version 130\n"
	"\n"
	"in vec4 vPosition;\n"
	"in vec4 vColor;\n"
	"\n"
	"out vec4 exColor;\n"
	"\n"
	"void main() {\n"
	"       gl_Position = vPosition;\n"
	"       exColor = vColor;\n"
	"}\n";

static char* frag_shaderprogram_pass = "#version 130\n"
	"\n"
	"in vec4 exColor;\n"
	"out vec4 fColor;\n"
	"\n"
	"void main() {\n"
	"       fColor = exColor;\n"
	"}\n";


///efun
ShaderProgram::ShaderProgram() : id(0) {
}

///efun
ShaderProgram::~ShaderProgram() {
}

///efun
ShaderProgram* ShaderProgram::newPassThrough() {
	ShaderProgram* sp = new ShaderProgram();
	sp->addBySourceCode(vert_shaderprogram_pass, GL_VERTEX_SHADER);
	sp->addBySourceCode(frag_shaderprogram_pass, GL_FRAGMENT_SHADER);
	sp->compile();
	return sp;
}

Shader::Shader(GLenum type, const char* code) {
	this->type = type;
	this->id = glCreateShader(this->type);
	const char* shaderSources[1];
	shaderSources[0] = code;

	glShaderSource(this->id, 1, (const GLchar**) shaderSources, NULL);
	glCompileShader(this->id);
	exitIfError();
}

void Shader::exitIfError() {
	GLint status;
	glGetShaderiv(this->id, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE) {
	    GLint infoLogLength;
	    glGetShaderiv(this->id, GL_INFO_LOG_LENGTH, &infoLogLength);

	    GLchar* strInfoLog =  (GLchar*) calloc(infoLogLength, sizeof(GLchar));
	    glGetShaderInfoLog(this->id, infoLogLength, NULL, strInfoLog);

	    fprintf(stderr, "Compilation error in shader(%d): %s\n", this->id, strInfoLog);
	    free(strInfoLog);
	    exit(EXIT_FAILURE);
	}
}

void ShaderProgram::addBySourceCode(const char* sourceCode, GLenum type) {
	Shader shader(type, sourceCode);
	shaders.push_back(shader);
}

void ShaderProgram::addByFilename(const char* filename, GLenum type) {
	FILE* file = fopen(filename, "r");
	if (file == NULL) {
		fprintf(stderr, "ERROR: could not find shader file: %s\n", filename);
		exit(EXIT_FAILURE);
	}
	fseek(file, 0L, SEEK_END);
	long file_size = ftell(file);
	rewind(file);
	char* buffer = (char*) calloc(file_size + 1, sizeof(char));
	bool read_ok = fread(buffer, file_size, 1, file);
	if (!read_ok) {
		fprintf(stderr, "ERROR: entire read fails: %s\n", filename);
	}
	fclose(file);

	addBySourceCode(buffer, type);

	free(buffer);
}

///efun
void ShaderProgram::compile() {
	this->id = glCreateProgram();
	for (size_t i = 0; i < shaders.size(); i++) {
		glAttachShader(this->id, this->shaders[i].id);
	}
	glLinkProgram(this->id);
}

///efun
void ShaderProgram::use() {
	if (this->id == 0) {
		fprintf(stderr, "ERROR: ShaderProgram not compiled\n");
		exit(EXIT_FAILURE);
	}
	glUseProgram(this->id);
}

///efun
void ShaderProgram::usePassThrough() {
	static ShaderProgram* program;
	if (program == NULL) {
		program = ShaderProgram::newPassThrough();
	}
	program->use();
}

///efun
void ShaderProgram::useNone() {
	glUseProgram(0);
}

