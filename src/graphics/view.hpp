#pragma once

#include "graphics/text.hpp"
#include "graphics/graph.hpp"
#include "graphics/boat.hpp"

#include <vector>

class ViewSubPlot {
	int index = 0;
	Graph* graph;
};

class View {
public:
	int width;
	int height;

	std::vector<Plot*> subPlots;

	///public
	View();
	~View();

	void addPlot(Plot* graph);

	void reshape(int width, int height);
	void draw();
	void resetViewport();
	void drawPlots();
	void drawPlot(Plot *plot, Dimension);
	void setViewport(Dimension dim);
};
