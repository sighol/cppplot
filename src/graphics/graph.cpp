#include "graph.hpp"

#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include "util/util.h"
#include <GL/freeglut.h>
#include <math.h>
#include "graphics/shaderprogram.hpp"


Graph::Graph(int historySize)
: m_history(historySize){
	this->m_graphMax = 0;
	this->m_graphMin = 0;
	this->m_colorShaderId = 1;
	this->m_positionShaderId = 0;

	this->m_graphColor = {255, 0, 0, 255};
}

Graph::~Graph() {}

void Graph::pushData(double value) {
	// Pushes the value to the history and updates the graph and axis_buffer
	m_history.push(value);
	computeMinMax(value);
	updateGraphBuffer();
	updateAxisBuffer();
}

void Graph::computeMinMax(double value) {
	if (value > this->m_graphMax) {
		this->m_graphMax = value;
	}
	if (value < this->m_graphMin) {
		this->m_graphMin = value;
	}
}

void Graph::setLine(double y) {
	m_extraBuffer.bind();
	Color line_color = {0, 255, 0, 255};
	float line = normalizeY(y);
	VertexData lines[2] = {
		{line_color,{-0.88, line, -0.05}},
		{line_color,{0.80, line, -0.05}},
	};
	pushToBuffer(lines, sizeof(lines));
}

void Graph::updateGraphBuffer() {
	m_graphBuffer.bind();
	int size = this->m_history.m_size;

	int vertex_size = sizeof(VertexData) * size;

	VertexData* vertices = getVertices();
	pushToBuffer(vertices, vertex_size);
	free(vertices);
}

VertexData* Graph::getVertices() {
	Color color = this->m_graphColor;
	VertexData* data = (VertexData*) calloc(this->m_history.m_size, sizeof(VertexData));
	int hist_size = m_history.size();

	// This is the current index of the history. Everything before this is new
	// data and everything after is old.
	int hist_index = m_history.m_size % m_history.allocated;

	for (int i = 0; i < hist_size; i++) {
		int index;
		if (i < hist_index) {
			// Data is new => should be put at the end of the graph
			index = hist_size - hist_index + i;
		} else {
			// Data is old => should be put at the beginning of the graph
			index = i - hist_index;
		}
		GLfloat x = Graph::normalizeX((double) index);
		GLfloat y = Graph::normalizeY(m_history.data[i]);
		GLfloat z = -0.1;
		data[index] = {
			color, {x, y, z}
		};
	}
	return data;
}

double Graph::normalizeY(double in) {
	// Returnes the y-value for use in opengl from the y-value in the history
	return normalize(in, this->m_graphMin, this->m_graphMax, -0.9, 0.9);
}

double Graph::normalizeX(double in) {
	// Returnes the x-value for use in opengl from the x-value in the history
	return normalize(in, 0, m_history.size(), -0.9, 0.8);
}

void Graph::pushToBuffer(VertexData* vertices, int vertex_size) {
	glBufferData(GL_ARRAY_BUFFER, vertex_size, &vertices[0], GL_DYNAMIC_DRAW);

	glVertexAttribPointer(
		this->m_colorShaderId,
		4,
		GL_UNSIGNED_BYTE,
		GL_TRUE,
		sizeof(VertexData), 0);

	glVertexAttribPointer(
		this->m_positionShaderId,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(VertexData),
		(void*)(sizeof(vertices[0].color)));

	glEnableVertexAttribArray(this->m_colorShaderId);
	glEnableVertexAttribArray(this->m_positionShaderId);
}

void Graph::updateAxisBuffer() {
	this->m_axisBuffer.bind();

	computeSteps();

	Color axis_color = {0, 0, 0, 255};
	Color hint_color = {200,200,200,255};

	// 4 vertices for the x and y axis, and 4 vertices for each of the axis
	// steps
	int vertex_count =  4 + 4 * this->m_axisStepsSize;
	int vertex_size = vertex_count * sizeof(VertexData);
	VertexData* vertices = (VertexData*) calloc(vertex_count, sizeof(VertexData));

	GLfloat axis_y_value = normalizeY(0.0);
	int axis_start = 4 * this->m_axisStepsSize;

	// The axis starts at the end of the array
	VertexData *axis = &vertices[axis_start];
	axis[0] = {axis_color, {-0.9, -1, 0}};
	axis[1] = {axis_color, {-0.9,  1, 0}};
	axis[2] = {axis_color, {-0.95,  axis_y_value, 0}};
	axis[3] = {axis_color, { 0.80,  axis_y_value, 0}};

	// The steps start at the beginning of the array
	VertexData* steps = &vertices[0];

	for (int i = 0; i < this->m_axisStepsSize; i++) {
		GLfloat line = normalizeY(this->m_axisSteps[i]);
		// Each step takes 4 positions. Need to jump 4 positions for each
		// axis_step;
		VertexData* step = &steps[i*4];
		step[0] = {axis_color,{-0.92, line, 0}};
		step[1] = {axis_color,{-0.88, line, 0}};
		step[2] = {hint_color, {-0.88, line, 0}};
		step[3] = {hint_color, { 0.80, line, 0}};
	}

	pushToBuffer(vertices, vertex_size);
	free(vertices);
}


void Graph::computeSteps() {
	double min = this->m_graphMin;
	double max = this->m_graphMax;

	double diff = max - min;

	// If the graph history is a straight line, we don't know where to place the
	// hint lines
	if (diff == 0.0) {
		this->m_axisStepsSize = 0;
		return;
	}

	double order = log(diff) / log(10);
	double int_part = (long) order;
	double float_part = order - int_part;
	double step_size = 1;

	// These number are found by trial and error so that the graph helper axises
	// are well placed.
	if (float_part < 0.1) {
		step_size = pow(10.0, int_part) * 0.1;
	} else if (float_part < 0.55) {
		step_size = pow(10.0, int_part) * 0.25;
	} else if (float_part < 0.85) {
		step_size = pow(10.0, int_part) * 0.5;
	} else {
		step_size = pow(10.0, int_part);
	}

	double current_step = 0;
	int i = 0;
	double* data = this->m_axisSteps;

	// determine steps below zero
	while (current_step >= min) {
		current_step -= step_size;
		data[i] = current_step;
		i++;
	}
	current_step = 0.0;
	// determine steps above zero
	while (current_step <= max) {
		current_step += step_size;
		data[i] = current_step;
		i++;
	}
	this->m_axisStepsSize = i;
}

void Graph::draw() {
	ShaderProgram::usePassThrough();

	m_axisBuffer.bind();
	glDrawArrays(GL_LINES, 0, 4+ 4 * this->m_axisStepsSize);

	m_graphBuffer.bind();
	int hist_size = m_history.size();
	glDrawArrays(GL_LINE_STRIP, 0, hist_size);

	m_extraBuffer.bind();
	glDrawArrays(GL_LINES, 0, 2);
}

void Graph::cleanHistory() {
	this->m_history.clean();
}
