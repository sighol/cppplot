#pragma once

#include <graphics/buffer.hpp>
#include <graphics/text.hpp>
#include "plot.hpp"

class Boat: public Plot {
private:
	double m_position;
	double m_setpoint;
	Buffer m_boatBuffer;
	Buffer m_setpointLineBuffer;
	Buffer m_axisBuffer;
	int m_axisCount;
	int m_axisMin;
	int m_axisMax;
	TextWriter* text;

public:
	Boat(void);
	~Boat();
	void draw(double m_setpoint, double m_position);
	void draw();
	void drawText(int width, int height);

	double position() const;
	void setPosition(double position);

	double setpoint() const;
	void setSetpoint(double setpoint);

private:

	void initBoatData();
	void initSetlineData();
	void initAxisData();
	double getAxisStepSize();
	void bufferData(size_t size, VertexData* data);
	double normalizeX(double x);
};
