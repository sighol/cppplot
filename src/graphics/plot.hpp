#ifndef PLOT_H
#define PLOT_H

#include <cstdio>

struct Dimension {
	double x, y, width, height;

	void debug() {
		printf("Dim: %lf, %lf, %lf, %lf\n", x, y, width, height);
	}
};


class Plot
{
public:
	Plot() {}
	virtual ~Plot() {}

	virtual void draw() = 0;
};

#endif // PLOT_H
