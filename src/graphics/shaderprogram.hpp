#pragma once

#include <GL/glew.h>
#include <vector>


struct Shader {
	GLuint id;
	GLenum type;

	Shader(GLenum type, const char* code);
	void exitIfError();
};

class ShaderProgram {
private:
	GLuint id;
	std::vector<Shader> shaders;

public:
	ShaderProgram();
	~ShaderProgram();
	void addBySourceCode(const char *sourceCode, GLenum type);
	void addByFilename(const char* filename, GLenum type);
	void compile();
	void use();

	static ShaderProgram* newPassThrough(void);
	static void usePassThrough();
	static void useNone();
};
