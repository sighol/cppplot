#pragma once

#include <GL/glew.h>
#include "graphics/buffer.hpp"
#include "text.hpp"
#include "history.hpp"
#include "plot.hpp"

class Graph : public Plot {
public:
	Buffer m_graphBuffer;
	Buffer m_axisBuffer;
	Buffer m_extraBuffer;

	double m_graphMax=0;
	double m_graphMin=0;
	GLuint m_colorShaderId;
	GLuint m_positionShaderId;
	double m_axisSteps[30];
	int m_axisStepsSize=0;

	Color m_graphColor;

	History m_history;

	void computeMinMax(double value);
	void updateGraphBuffer();
	VertexData* getVertices();
	double normalizeY(double in);
	double normalizeX(double in);
	void pushToBuffer(VertexData* vertices, int vertex_size);
	void updateAxisBuffer();
	void computeSteps();
	void print(VertexData* data, int size);
public:
	Graph(int historySize=500);
	~Graph();
	void pushData(double value);
	void setLine(double y);
	void draw();
	void drawText(int width, int height);
	void cleanHistory();
};
