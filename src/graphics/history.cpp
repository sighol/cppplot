#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include "history.hpp"

///private
///endprivate

History::History(int allocated) {
	this->allocated = allocated;
	this->m_size = 0;
	this->data = (double*) calloc(this->allocated, sizeof(double));
}

History::~History() {
	free(this->data);
}

void History::push(double value) {
	this->data[this->m_size % this->allocated] = value;
	this->m_size++;

	// Modulo is slower when the input is large.
	// Allocated is multiplied by 2 since some previous history is required for
	// history_size to return the correct value.
	if (this->m_size > this->allocated * 2) {
		this->m_size -= this->allocated;
	}
}

int History::size() {
	return std::min(this->allocated, this->m_size);
}

int History::get(int size) {
	return this->data[size % this->allocated];
}

void History::print() {
	for (int i = 0; i < this->allocated; i++) {
		printf("%f, ", this->data[i]);
	}
	printf("\n");
}

void History::clean() {
	for (int i = 0; i < this->allocated; i++) {
		this->data[i] = 0;
	}
	this->m_size = 0;
}
