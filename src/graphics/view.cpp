#include <GL/glew.h>
#include <GL/freeglut.h>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>

#include "graphics/shaderprogram.hpp"

#include "view.hpp"


View::View() {
}

View::~View() {
}

void View::addPlot(Plot *graph)
{
	subPlots.push_back(graph);
}

void View::reshape(int width, int height) {
	this->width = width;
	this->height = height;
}

void View::draw() {
	// Draws everything on the screen
	resetViewport();
	glClearColor(0.9, 0.9, 0.9, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	drawPlots();
}

void View::resetViewport() {
	glScissor(0, 0, this->width, this->height);
	glViewport(0, 0, this->width, this->height);
}

void View::drawPlots()
{
	int size = subPlots.size();
	int rows = ceil(sqrt(size));
	int cols = floor(sqrt(size));
	if (rows * cols < size) {
		cols++;
	}

	double marginWidth = 0.05 * width;
	double marginHeight = 0.05 * height;
	double plotWidth = this->width / cols;
	double plotHeight = this->height / rows;

	for (int n = 0; n < size; n++) {
		int row = floor(n / rows);
		int col = n % rows;
		Dimension dim = {row*plotWidth+marginWidth/2, col*plotHeight+marginHeight/2, plotWidth-marginWidth, plotHeight-marginHeight};
		drawPlot(subPlots[n], dim);
	}
}

void View::drawPlot(Plot *plot, Dimension dim) {
	setViewport(dim);

	glClearColor(1.0, 1.0, 1.0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	plot->draw();
}



void View::setViewport(Dimension dim)
{
	glScissor((GLint)dim.x, (GLint)dim.y, (GLint)dim.width, (GLint)dim.height);
	glViewport((GLint)dim.x, (GLint)dim.y, (GLint)dim.width, (GLint)dim.height);
}
