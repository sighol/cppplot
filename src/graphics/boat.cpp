#include <stdlib.h>
#include <stdio.h>
#include <graphics/shaderprogram.hpp>
#include <GL/freeglut.h>
#include "util/util.h"


#include "boat.hpp"



double Boat::position() const
{
	return m_position;
}

void Boat::setPosition(double position)
{
	m_position = position;
}

double Boat::setpoint() const
{
	return m_setpoint;
}

void Boat::setSetpoint(double setpoint)
{
	m_setpoint = setpoint;
}
Boat::Boat() {
	this->m_axisCount = 5;
	this->m_axisMin = 0;
	this->m_axisMax = 2000;

	this->text = text_new(GLUT_BITMAP_HELVETICA_12);

	initBoatData();
	initAxisData();
}

///efun
Boat::~Boat() {
	text_destroy(this->text);
}

///pfun
void Boat::initBoatData() {
	Color boat_color = {0, 50, 100, 255};

	// Initiating boat.
	VertexData nodes[] = {
		{boat_color, { 0,  0, 0}}, {boat_color, { 3, -4, 0}},
		{boat_color, {15, -4, 0}}, {boat_color, {15, -3, 0}},
		{boat_color, {16, -3, 0}}, {boat_color, {16,  0, 0}},
		{boat_color, {13,  0, 0}}, {boat_color, {13,  4, 0}},
		{boat_color, { 9,  4, 0}}, {boat_color, { 8,  0, 0}},
	};

	VertexData boat[] = {
		nodes[9], nodes[0], nodes[1],
		nodes[2], nodes[3], nodes[4],
		nodes[5], nodes[6], nodes[7],
		nodes[8],
	};

	double boat_size_nodes = 16;
	double boat_size_gl = 0.5;
	double boat_scale = boat_size_gl / boat_size_nodes;

	// Scales and positions the boat into place
	double x_position = normalizeX(this->m_position);
	for (int i = 0; i < sizeof(boat)/sizeof(VertexData); i++) {
		boat[i].position.x = boat[i].position.x *  boat_scale + x_position;
		boat[i].position.y = boat[i].position.y *  boat_scale;
	}

	this->m_boatBuffer.bind();
	bufferData(sizeof(boat), boat);
}

///pfun
void Boat::initSetlineData() {
	Color line_color = {0, 150, 0, 255};
	double line_x = normalizeX(this->m_setpoint);
	VertexData line[] = {
		{line_color, {line_x, -1, 0}},
		{line_color, {line_x,  1, 0}},
	};

	this->m_setpointLineBuffer.bind();
	bufferData(sizeof(line), line);
}

///pfun
void Boat::initAxisData() {
	double axis_step_size = getAxisStepSize();

	VertexData axis[this->m_axisCount*2];
	Color axis_color = {255, 255, 255, 255};

	double step = this->m_axisMin;
	for (int i = 0; i < this->m_axisCount; i++) {
		double x = normalizeX(step);
		axis[i*2] = (VertexData) {axis_color, {x, -1, 0}};
		axis[i*2+1] = (VertexData) {axis_color, {x, 1, 0}};
		step += axis_step_size;
	}

	this->m_axisBuffer.bind();
	bufferData(sizeof(axis), axis);
}

///pfun
double Boat::getAxisStepSize() {
	return (this->m_axisMax - this->m_axisMin) / (this->m_axisCount-1);
}

///pfun
void Boat::bufferData(size_t size, VertexData* data) {
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (void*) sizeof(Color));
	glVertexAttribPointer(1, 3, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexData), 0);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
}

///pfun
double Boat::normalizeX(double x) {
	return normalize(x, 0, 2500, 0.3, -1);
}

///efun
void Boat::draw(double setpoint, double position) {
	this->m_setpoint = setpoint;
	this->m_position = position;
	ShaderProgram::usePassThrough();

	initBoatData();
	initSetlineData();

	this->m_boatBuffer.bind();
	glDrawArrays(GL_TRIANGLE_FAN, 0, 10);

	this->m_setpointLineBuffer.bind();
	glDrawArrays(GL_LINES, 0, 2);

	this->m_axisBuffer.bind();
	glDrawArrays(GL_LINES, 0, this->m_axisCount*2);
}

void Boat::draw()
{
	draw(m_setpoint, m_position);
}

///efun
void Boat::drawText(int width, int height) {
	double axis_step_size = getAxisStepSize();
	double step = this->m_axisMin;

	for (int i = 0; i < this->m_axisCount; i++) {
		double y = height - height/7;

		double x_gl = normalizeX(step);
		double x =  (x_gl + 1) * width/2;

		char str[15];
		sprintf(str, "%5.0f", step);
		text_draw_string(this->text, (int)x, (int)y, str);
		step += axis_step_size;
	}
}
