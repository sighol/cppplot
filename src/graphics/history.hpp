#pragma once

class History {
public:
	double* data;
	int m_size;
	int allocated;

public:
	History(int allocated);
	~History();
	void push(double value);
	int size();
	int get(int index);
	void clean();

private:
	void print();
};
