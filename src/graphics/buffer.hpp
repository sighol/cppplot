#pragma once

#include <GL/glew.h>


struct Color {
	GLubyte r, g, b, a;
};

struct Position{
	GLfloat x, y, z;
};

struct VertexData{
	Color color;
	Position position;
};

class Buffer {
public:
	GLuint buffer_id;
	GLuint vao_id;
public:
	Buffer();
	~Buffer();
	void bind();
	void printVertexData(size_t size, VertexData* data);
};

