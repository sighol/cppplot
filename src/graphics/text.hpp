#pragma once

#include <GL/glew.h>

typedef struct {
	GLubyte r, g, b;
} TextColor;

typedef struct {
	GLint glyphs_display_list;
	TextColor color;
} TextWriter;


///public
TextWriter* text_new(void* bitmap);
void text_destroy(TextWriter* self);
void text_draw_string(TextWriter* self, int x, int y, const char * text);
///endpublic
