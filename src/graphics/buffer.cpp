#include "graphics/buffer.hpp"

#include <stdlib.h>
#include <stdio.h>



Buffer::Buffer()
: vao_id(0)
, buffer_id(0) {
	glGenVertexArrays(1, &this->vao_id);
	glGenBuffers(1, &this->buffer_id);
}

Buffer::~Buffer() {
	glDeleteVertexArrays(1, &this->vao_id);
	glDeleteBuffers(1, &this->buffer_id);
}

void Buffer::bind() {
	glBindVertexArray(this->vao_id);
	glBindBuffer(GL_ARRAY_BUFFER, this->buffer_id);
}

void Buffer::printVertexData(size_t size, VertexData* data) {
	for (size_t i = 0; i < size; i++) {
		Color* c = &data[i].color;
		Position* p = &data[i].position;
		printf("Color: {%d, %d, %d}, ", c->r, c->g, c->b);
		printf("Position: {%f, %f, %f}\n", p->x, p->y, p->z);
	}
}
