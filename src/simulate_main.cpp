#include "simulate_main.h"
#include <Eigen/Dense>


Main* main_global;

///efun
int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	graphicsInit(800, 600);
	main_global = new Main();
	glutCloseFunc(close_callback);
	glutDisplayFunc(display_callback);
	glutIdleFunc(idle_callback);
	glutReshapeFunc(reshape_callback);
	glutKeyboardFunc(keyboard_callback);

	glutMainLoop();
};

///efun
Main::Main() {
	this->width = 800;
	this->height = 600;
	this->next = 0;
	this->setpoint = 0;
	this->elapsed_time = 0;

	this->positionGraph.m_graphColor = {255, 0, 0};
	this->velocityGraph.m_graphColor = {0, 200, 0};
	this->powerGraph.m_graphColor = {0, 0, 255};

	this->view.addPlot(&this->positionGraph);
	this->view.addPlot(&this->velocityGraph);
	this->view.addPlot(&this->powerGraph);
	this->view.addPlot(&this->boatPlot);
}

///efun
void keyboard_callback(unsigned char key, int x, int y) {
	Main* self = main_global;
    if (key == 27) {
        exit(0);
    }

    if (key == GLUT_KEY_F) {
		toggleFullscreen(self->width, self->height);
    }

    if (key == GLUT_KEY_0) {
    	self->setSetpoint(0);
    } else if(key == GLUT_KEY_1) {
    	self->setSetpoint(500);
    } else if (key == GLUT_KEY_2) {
    	self->setSetpoint(1000);
    } else if (key == GLUT_KEY_3) {
    	self->setSetpoint(1500);
    } else if (key == GLUT_KEY_4) {
    	self->setSetpoint(2000);
    }
}

///efun
void reshape_callback(int width, int height) {
	Main* self = main_global;
	self->view.reshape(width, height);
    glViewport(0, 0, width, height);
    glScissor(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, height, 0, /*near=*/0, /*far=*/1);

    glutPostRedisplay();
}

///efun
void idle_callback(void) {
	glutPostRedisplay();
}

///efun
void display_callback(void) {

	Main* self = main_global;
	printFps();
	simulate_controller(self);

	self->view.draw();

	glutSwapBuffers();
	glFlush();
}

///efun
void simulate_controller(Main* self) {
	static double integrator = 0;
	static double velocity = 0;
	static double pos = 0;

	double diff = self->setpoint - pos;

	double Kp = 4.8;
	double Ki = 0.2;
	double Kd = 100;

	if (diff > 100/Kp) {
		integrator += 100/Kp;
	} else {
		integrator += diff;
	}

	if (integrator < 0) {
		integrator = 0;
	}

	double power_p = Kp * diff;
	double power_i = Ki * integrator;
	double power_d = Kd * velocity;

	double power = power_p + power_i - power_d;

//	if (power > 800) {
//		power = 800;
//	}
//	if (power < -100) {
//		power = -100;
//	}


	self->velocityGraph.pushData(velocity);
	self->powerGraph.pushData(power);
	self->positionGraph.pushData(pos);
	self->boatPlot.setPosition(pos);

	simulate_apply_power(self, &pos, &velocity, power);

	self->power_p = power_p;
	self->power_i = power_i;
	self->power_d = power_d;
	self->power = power;
}
///efun
void simulate_apply_power(Main* self, double* pos, double* vel, double power) {
	static const double M = 300.3;
	static const double B11 = 40;
	static const double line_drag = 400;


	double A11 = M;
	double MA = A11 + M;

	Eigen::Matrix2d A;
	A << 0, 1, 0, -B11/MA;

	Eigen::Vector2d B;
	B << 0, 1.0/MA;

	Eigen::Vector2d x;
	x << *pos, *vel;

	Eigen::Vector2d x_diff = A*x + B*power - B * line_drag;
	Eigen::Vector2d x_new = x + x_diff;

	*pos = x_new(0);
	*vel = x_new(1);
	self->next = *pos;

}

///efun
void close_callback(void) {
	delete main_global;
}

double Main::getSetpoint() const
{
	return setpoint;
}

void Main::setSetpoint(double value)
{
	setpoint = value;
	boatPlot.setSetpoint(value);
	positionGraph.setLine(value);
}
