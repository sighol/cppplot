#pragma once

#include <phidget21.h>

typedef struct {
	CPhidgetServoHandle servo;
} Motor;

///public
Motor* motor_new(void);
void motor_start(Motor* self);
void motor_destroy(Motor* self);
void motor_set_power(Motor* self, double power);
///endpublic
