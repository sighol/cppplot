#pragma once

#include <phidget21.h>


typedef struct {
	CPhidgetInterfaceKitHandle ifKit;

	int distance;
	int last_value;
} Sensor;


///public
Sensor* sensor_new(void);
void sensor_destroy(Sensor* self);
///endpublic
