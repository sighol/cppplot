#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>

#include "derivator.h"

///private
double derivator_get_time(void);
///endprivate

///efun
Derivator* derivator_new(int max_size) {
	Derivator* self = (Derivator*)calloc(1, sizeof(Derivator));
	self->max_size = max_size;
	self->history = (HistElement*) calloc(max_size, sizeof(HistElement));
	self->size = 0;
	return self;
}

///efun
void derivator_destroy(Derivator* self) {
	free(self->history);
	free(self);
}

///efun
void derivator_push(Derivator* self, int value) {
	HistElement h = {(double) value, derivator_get_time()};
	int index = self->size % self->max_size;
	self->history[index] = h;
	self->size++;
}

///pfun
double derivator_get_time(void) {
	struct timeval time;
	double mtime, seconds, useconds;
	gettimeofday(&time, NULL);
	seconds = time.tv_sec;
	useconds = time.tv_usec;
	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
	return mtime;
}

///efun
double derivator_speed(Derivator* self) {
	if (self->size == 0) {
		return 0;
	}

	int lastIndex = (self->size - 1)% self->max_size;
	int firstIndex = self->size % self->max_size;

	HistElement first = self->history[firstIndex];
	HistElement last = self->history[lastIndex];

	double value_diff = last.data - first.data;
	double time_diff = last.time - first.time;

	return value_diff / time_diff;
}

///efun
void derivator_print(Derivator* self) {
	printf("[");
	for (int i = 0; i < self->max_size; i++) {
		printf("%3f, ", self->history[i].data);
	}
	printf("]\n");
}
