#pragma once

#include <stdlib.h>
#include <stdio.h>

typedef struct Matrix {
	double * matrix;
	int rows, cols;
} Matrix;

///public
Matrix* matrix_new(int rows, int cols);
void matrix_destroy(Matrix* self);
void matrix_input(Matrix* self, double* matrix);
Matrix* matrix_new_input(int rows, int cols, double* matrix);
void matrix_print(Matrix* self);
Matrix* matrix_multiply(Matrix* self, Matrix* other);
Matrix* matrix_add(Matrix* self, Matrix* other);
Matrix* matrix_sub(Matrix* self, Matrix* other);
Matrix* matrix_multiply_scalar(Matrix* self, double scalar);
double matrix_get(Matrix* self, int row, int col);
///endpublic
