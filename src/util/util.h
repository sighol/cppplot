#pragma once

///public
double normalize(double input, double in_min,  double in_max,
							   double out_min, double out_max);
///endpublic
