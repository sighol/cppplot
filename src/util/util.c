#include "util.h"

///private
///endprivate

///efun
double normalize(double input, double in_min,  double in_max,
							   double out_min, double out_max) {
	double percentage_done = (input - in_min) / (in_max - in_min);
	return out_min + (out_max - out_min) * percentage_done;
}
