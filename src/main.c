#include "main.h"

///private
///endprivate

Main* main_global;

///efun
int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	Main* self = main_new();
	main_global = self;
	glutDisplayFunc(main_display_callback);
	glutReshapeFunc(main_reshape_callback);
	glutKeyboardFunc(keyboard_callback);
	glutCloseFunc(main_close_callback);
	glutIdleFunc(main_idle_callback);
	glutMainLoop();
	return 0;
}

///efun
Main* main_new(void) {
	Main *self = (Main*) calloc(1, sizeof(Main));

	int width = 1000, height = 700;
	int setpoint = 1000;

	graphics_init(width, height);

	self->controller = controller_new(setpoint);
	self->view = new View();
	self->view->setpoint = setpoint;

	self->view->reshape(width, height);
	return self;
}

///efun
void main_destroy(Main* self) {
	controller_destroy(self->controller);
	delete self->view;
	free(self);
}

///efun
void main_display_callback(void) {
	main_global->view->draw();

	main_run_controller(main_global);
	main_push_graph_data(main_global);
	print_fps();

	glutSwapBuffers();
	glFlush();
}

///efun
void main_run_controller(Main* self) {
	controller_begin(self->controller);
	// controller_add_lqr(self->controller);
	controller_add_pid(self->controller);
	controller_end(self->controller);
}

///efun
void main_push_graph_data(Main* self) {
	double speed = self->controller->speed;
	double power = self->controller->power;
	double distance = self->controller->sensor->distance;

	self->view->velocity_graph->push_data(speed);
	self->view->power_graph->push_data(	power);
	self->view->distance_graph->push_data(distance);

	self->view->position = distance;
	self->view->setpoint = self->controller->setpoint;

	self->view->fields_begin();
	self->view->set_field("Power_P", self->controller->power_p);
	self->view->set_field("Power_I", self->controller->power_i);
	self->view->set_field("Power_D", self->controller->power_d);
}

///efun
void main_reshape_callback (int width, int height) {
	Main* self = main_global;
	self->view->reshape(width, height);

	glViewport(0, 0, width, height);
	glScissor(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width, height, 0, /*near=*/0, /*far=*/1);

	glutPostRedisplay();
}

///efun
void keyboard_callback(unsigned char key, int x, int y) {
	Main* self = main_global;

	if (key == GLUT_KEY_ESCAPE) {
		exit(EXIT_SUCCESS);
	}

	if (key == GLUT_KEY_F) {
		toggle_fullscreen(self->view->width, self->view->height);
	}

	if (key == GLUT_KEY_0) {
		self->controller->setpoint = 100;
	} else if (key == GLUT_KEY_1) {
		self->controller->setpoint = 500;
	} else if (key == GLUT_KEY_2) {
		self->controller->setpoint = 1000;
	} else if (key == GLUT_KEY_3) {
		self->controller->setpoint = 1500;
	} else if (key == GLUT_KEY_4) {
		self->controller->setpoint = 2000;
	}

	if (key == GLUT_KEY_C) {
		main_history_clean(self);
	}
}
///efun
void main_history_clean(Main* self) {
	self->view->clean_history();
}

///efun
void main_idle_callback(void) {
	glutPostRedisplay();
}

///efun
void main_close_callback(void) {
	Main* self = main_global;
	main_write_to_file(self);

	main_exit(main_global);
}

///efun
void main_write_to_file(Main* self) {
	char buff[70];
	time_t rawtime;
	struct tm* my_time;
	time(&rawtime);
	my_time = localtime(&rawtime);

	strftime(buff, sizeof buff, "runs/%Y-%m-%d_%H-%M-%S.run", my_time);

	self->view->write_to_file(buff);
}

///efun
void main_exit(Main* self) {
	main_destroy(self);
}
