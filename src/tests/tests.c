#include "minunit.h"
#include <stdio.h>

#include "graphics/text.h"

///private
static char * test_text_init_destroy();
static char * all_tests();
int main(int argc, char **argv);
///endprivate

int tests_run = 0;

int foo = 7;
int bar = 4;

///pfun
static char * test_text_init_destroy() {
	TextWriter* text = text_new(GLUT_BITMAP_HELVETICA_12);
	text_destroy(text);
	return 0;
}

///pfun
static char * all_tests() {
	mu_run_test(test_text_init_destroy);
	return 0;
}

///pfun
int main(int argc, char **argv) {
	glutInit(&argc, argv);
	char *result = all_tests();
	if (result != 0) {
		printf("%s\n", result);
	} else {
		printf("ALL TESTS PASSED\n");
	}
	printf("Tests run: %d\n", tests_run);

	return result != 0;
}