#include "minunit.h"
#include <stdio.h>

#include "graphics/graph.h"

///private
///endprivate

int tests_run = 0;

static char* test_graph_init_destroy() {
	Graph* self = graph_new(10);
	// graph_destroy(self);
	return 0;
}


static char * all_tests() {
	mu_run_test(test_graph_init_destroy);
	return 0;
}

int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glewExperimental = 1;
	graphics_init(100, 100);
	char *result = all_tests();
	if (result != 0) {
		printf("FAIL: %s\n", result);
	} else {
		printf("ALL TESTS PASSED\n");
	}
	printf("Tests run: %d\n", tests_run);

	return result != 0;
}